import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BMICalculatorTest {

	/*		R1:       BMI   18.5 - 25 = normal
			R2:       BMI   26 - 30 - average
			R3:       BMI  31-40 - fat
			R4:       BMI 41+ -- severe fat*/

	@Test
	public  void testBMINormal() {
		BMICalculator c = new BMICalculator() ;
		assertEquals("Normal", c.bmi(20, 1));
	}


@Test
public  void testBMIAverage() {
	BMICalculator c = new BMICalculator() ;
	assertEquals("Average", c.bmi(27,1));
	
}
@Test
public  void testBMIFat() {
	BMICalculator c = new BMICalculator() ;
	assertEquals("Fat", c.bmi(32,1));
}

@Test
public  void testBMISevereFat() {
	BMICalculator c = new BMICalculator() ;
	assertEquals("SevereFat", c.bmi(45,1));
}

}
