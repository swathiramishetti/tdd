import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EvenNumbersTest {

	/* function isEven(n) {}
	 * Accepts 1 integer value, n
	 * 
	 * R1. If n is even, return true
	 * R2. If n is odd, return false
	 * R3. If N < 1, then return false
	 * R4. N must be > 1
	 */

	@Test
	 // R1. If n is evenn, return true

	public void testEvenNumber() {
		EvenNumbers e = new EvenNumbers();
		
		
assertTrue(e.isEven(6));
	}



@Test
// R2. If n is odd, return true

public void testOddNumber() {
	EvenNumbers e = new EvenNumbers();
	
	
assertFalse(e.isEven(31));
}


@Test
// R3. If N < 1, then return false

public void testNumberLessthanOne() {
	EvenNumbers e = new EvenNumbers();
	
	
assertFalse(e.isEven(-8));

}
@Test
//R3. If N < 1, then return false

public void testNumberGreaterthanOne() {
	EvenNumbers e = new EvenNumbers();
	
	
assertTrue(e.isEven(90));
}

}